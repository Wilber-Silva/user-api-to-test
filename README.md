# Description

- Aplicação criada usando node __v14.17.0__
- Foi usado como framework principal o __NestJs__ e __TypeOrm__
- Banco de dados __postgres__
- A documentação mais detalhada esta no __swagger__, o mesmo foi criado usando os decorators do NestJs pensando em não precisar de um __postman__.

Adotei o nestjs e typeorm pois gosto bastante dos mesmos e pela facilidade de usar o swagger com nestjs, tendo em vista também as funcionalidades do typecript que me obriga a manter a documentação do swagger atualizada.

Deixei o __.env__ do projeto commitado e estou usando __dotenv__ (geralmente não usado ele em aplicações reais) para facilitar o start da aplicação e também um __docker-compose__ com as configurações do postgresql que foram usadas.

A aplicação vai startar em modo __debug__ por conta do parâmetro que pode ser encontrado na __.env__ e assim todas as query vão aparecer no console.

A aplicação foi inspirada em um serviço já existem claro que é uma versão bem resumida do mesmo, tentei trazer __conhecimentos variados__ como utilização de decorator e criação dos mesmos, algumas das letrinhas do __solid__, código mais limpo e organizado de um jeito que acredito ser limpo e organizado.

As pastas principais do projeto são __Http__ e __Database__, como os nomes sugerem Http é onde ficam todos os módulos que vão servir ao protocolo http e database é tudo o que diz respeito ao banco de dados (conifg, entidades, repositórios, scripts de migração, scripts de seeders). As demais pastas são mais pra organizar interfaces, enum, e coisas que podem ser compartilhadas para toda a aplicação ou que são usadas no módulo principal

## Installation

```bash
npm install
```

ou

```bach
yarn install
```

Aqui eu usei o __yarn__ e como pode ter alguns __scripts__ no __package.json__ usando ele caso queira testa-los recomendo usar o yarn para instalação das dependências do projeto

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

Também está configurado o modo debug para funcionar com o __vsCode__ ao clicar no botão play do debug da ide o mesmo executara

```bash
#
$ yarn start:ts:dev
```

## Test

```bash
# unit tests
$ yarn test

# test coverage
$ yarn test:cov
```

Como o intuito de criar módulos é facilitar o desmembramento de um projeto acredito que o mesmo não deve levar em conta apenas as features mas os testes também então costumo criar os testes junto com as classes mantendo o mesmo nome e adicionando a extensão do teste no arquivo.

Para optimização de tempo como estou alocado atualmente não deixei os testes com 100% cobertura usei __TDD__ apenas no primeiro método criado e ainda sim só até o user.service.ts

## Swagger UI

Após o start da aplicação com sucesso
[clique aqui](http://localhost:3000/api) para abrir o swagger ui - local

Para fazer a requisição as apis talvez seja necessário usar o botão "Authorize" verde que fica no canto superior direito da página algumas vezes ele reclama mas é só preencher o value no modal que vai abrir e pronto

Na __.env__ tem uma variável chamada __APP_KEYS__ onde está os tokens de autenticação para conseguir chamar as apis, o próprio swagger vai pedir uma das keys que estão lá, todas as apis são por default privadas sendo necessário que use um decorator para tornar a api como publica

### Commit

Os commits do projeto estão usando padrão de commit, tem o __hucky__ e o __commitlint__ para travar commmits fora de padrão

Ficou configurado 3 formas de fazer um commit

```bash
yarn c
```

ou

```bash
yarn commit
```

 ou

```bash
git commit
```

## Nest

Caso tenha algum problema no start da aplicação [clique aqui](https://github.com/nestjs/nest).
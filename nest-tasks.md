# Annotations

- create seeders (Permissions[add permissions when create a new feat])
- finish auth: forgotPassword, recoverPassword, logout
- user remove creates from user http module
- attendance create/update/delete/list
- patient create/update/delete/edit
- address-patient create/update/delete/edit

import { Gender } from '@Enum';
import { IUserModel } from '@Protocols/Model';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('user')
export class UserEntity implements IUserModel {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', length: 100, nullable: false })
  username: string;

  @Column({ type: 'varchar', length: 30, nullable: true })
  firstName?: string;

  @Column({ type: 'varchar', length: 60, nullable: true })
  lastName?: string;

  @Column({ type: 'varchar', length: 50, nullable: true })
  nickname?: string;

  @Column({ type: 'varchar', length: 100, nullable: false })
  login: string;

  @Column({ type: 'varchar', length: 255, nullable: false, select: false })
  password?: string;

  @Column({ type: 'enum', enum: Gender, nullable: false })
  gender: Gender;

  @Column({ type: 'int', nullable: false })
  age: number;

  @CreateDateColumn()
  createdAt: string | Date;

  @UpdateDateColumn()
  updatedAt: string | Date;

  @DeleteDateColumn()
  deletedAt: string | Date | null;
}

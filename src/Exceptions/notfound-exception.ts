import { IException } from '@Protocols/exception.interface';
import { HttpStatus } from '@nestjs/common';
import { BaseException } from './base.exception';

export class NotfoundException extends BaseException {
  constructor(params: IException) {
    params.statusCode = HttpStatus.NOT_FOUND;
    super(params);
  }
}

import { IException } from '@Protocols/exception.interface';
import { HttpStatus } from '@nestjs/common';
import { BaseException } from './base.exception';

export class UnauthorizedException extends BaseException {
  constructor(params: IException) {
    params.statusCode = HttpStatus.UNAUTHORIZED;
    super(params);
  }
}

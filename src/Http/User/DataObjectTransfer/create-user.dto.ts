import { Gender } from '@Enum';
import { ApiProperty } from '@nestjs/swagger';
import { IUserSchema } from '@Protocols/Schema';
import {
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';

export class CreateUserDto implements IUserSchema {
  @ApiProperty({ type: String, format: 'string', required: true })
  @IsNotEmpty({ message: 'Username cannot empty' })
  @IsString({ message: 'Username must be a string' })
  username: string;

  @ApiProperty({ type: String, format: 'string', required: false })
  @IsOptional()
  @IsString({ message: 'FirstName must be a string' })
  firstName?: string;

  @ApiProperty({ type: String, format: 'string', required: false })
  @IsOptional()
  @IsString({ message: 'FirstName must be a string' })
  lastName?: string;

  @ApiProperty({ type: String, format: 'string', required: false })
  @IsOptional()
  @IsString({ message: 'FirstName must be a string' })
  nickname?: string;

  @ApiProperty({ type: String, format: 'string', required: false })
  @IsString({ message: 'Login must be a string' })
  login: string;

  @ApiProperty({ type: String, format: 'string', required: true })
  @IsString({ message: 'Password must be a string' })
  @MinLength(6, { message: 'Password must be at least 6 characters' })
  password: string;

  @ApiProperty({ type: String, enum: Gender, format: 'string', required: true })
  @IsEnum(Gender, { message: 'Gender is not valid' })
  gender: Gender;

  @ApiProperty({ type: Number, format: 'number', required: true })
  @IsNumber(
    { allowInfinity: false, allowNaN: false },
    { message: 'Age must be a valid number' },
  )
  age: number;
}

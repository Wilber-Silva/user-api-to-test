import { ApiProperty } from '@nestjs/swagger';
import { IPathParamId } from '@Protocols/HttpRequest';
import { IsUUID } from 'class-validator';

export class PathParamUserIdDto implements IPathParamId {
  @ApiProperty({ type: String, format: 'uuid', required: true })
  @IsUUID('4')
  id: string;
}

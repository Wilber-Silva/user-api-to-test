import { Gender } from '@Enum';
import { ApiProperty } from '@nestjs/swagger';
import { ISearchWithPaginationResults } from '@Protocols/HttpRequest';
import { IUserModel } from '@Protocols/Model';

export class SearchUsersWithPaginationDto
  implements ISearchWithPaginationResults, IUserModel
{
  @ApiProperty({ type: Number, required: true, default: 10 })
  take: number;

  @ApiProperty({ type: Number, required: true, default: 1 })
  page: number;

  @ApiProperty({ type: String, format: 'string', required: false })
  username: string;

  @ApiProperty({ type: String, format: 'string', required: false })
  firstName?: string;

  @ApiProperty({ type: String, format: 'string', required: false })
  lastName?: string;

  @ApiProperty({ type: String, format: 'string', required: false })
  nickname?: string;

  @ApiProperty({ type: String, format: 'string', required: false })
  login: string;

  password?: string;

  @ApiProperty({
    type: String,
    enum: Gender,
    format: 'string',
    required: false,
  })
  gender: Gender;

  @ApiProperty({ type: Number, format: 'number', required: false })
  age: number;

  @ApiProperty({ type: String, format: 'uuid', required: false })
  id: string;

  @ApiProperty({ type: String, format: 'timestamp', required: false })
  createdAt: string | Date;

  @ApiProperty({ type: String, format: 'timestamp', required: false })
  updatedAt: string | Date;

  @ApiProperty({ type: String, format: 'timestamp', required: false })
  deletedAt: string | Date | null;

  [key: string]: any;
}

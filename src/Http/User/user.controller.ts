import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import {
  CreateUserDto,
  PathParamUserIdDto,
  SearchUsersWithPaginationDto,
  UpdateUserDto,
} from './DataObjectTransfer';
import { IHttpResponse } from '@Protocols/HttpResponse';
import { UserService } from './user.service';
import { IUserModel } from '@Protocols/Model';
import { IPaginationResultUsecase } from '@Protocols/Usecase';

@ApiTags('user')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @ApiOperation({
    description: 'Create new user.',
    tags: ['user', 'post'],
  })
  @ApiHeader({ name: 'X-API-KEY', required: true })
  @ApiBody({ type: CreateUserDto })
  @ApiResponse({ status: HttpStatus.CREATED, type: CreateUserDto })
  async createNewUser(
    @Body() createUserDto: CreateUserDto,
  ): Promise<IHttpResponse<IUserModel>> {
    return {
      data: await this.userService.newUser(createUserDto),
    };
  }

  @Put('/:id')
  @ApiOperation({
    description: 'Update user information.',
    tags: ['user', 'put'],
  })
  @ApiHeader({ name: 'X-API-KEY', required: true })
  @ApiBody({ type: UpdateUserDto })
  @ApiResponse({ status: HttpStatus.OK })
  async updateUserData(
    @Param() pathParam: PathParamUserIdDto,
    @Body() userDto: UpdateUserDto,
  ): Promise<void> {
    await this.userService.updateUserInformation(pathParam.id, userDto);
  }

  @Get('/:id')
  @ApiOperation({
    description: 'Get user by id.',
    tags: ['user', 'get'],
  })
  @ApiHeader({ name: 'X-API-KEY', required: true })
  @ApiResponse({ status: HttpStatus.OK })
  async getUserById(
    @Param() pathParam: PathParamUserIdDto,
  ): Promise<IUserModel> {
    return this.userService.getOneUserById(pathParam.id);
  }

  @Get()
  @ApiOperation({
    description: 'Get users with pagination.',
    tags: ['user', 'get'],
  })
  @ApiHeader({ name: 'X-API-KEY', required: true })
  @ApiResponse({ status: HttpStatus.OK })
  async getUsersWithPatination(
    @Query() query: SearchUsersWithPaginationDto,
  ): Promise<IPaginationResultUsecase<IUserModel>> {
    return this.userService.getUsersWithPagination(query);
  }
}

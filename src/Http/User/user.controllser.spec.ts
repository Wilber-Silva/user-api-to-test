import { Test, TestingModule } from '@nestjs/testing';
import faker from 'faker';
import { UserController } from './user.controller';
import { Gender } from '@Enum';
import { UserService } from './user.service';
import { UserUseCasesModule } from '@UseCases/User/user.usecase.module';

describe('UserController', () => {
  let userController: UserController;
  let userService: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [UserUseCasesModule],
      controllers: [UserController],
      providers: [UserService],
    }).compile();

    userController = module.get<UserController>(UserController);
    userService = module.get<UserService>(UserService);
  });

  describe('root', () => {
    it('should create and return created user', async () => {
      const firstName = faker.name.firstName();
      const lastName = faker.name.lastName();
      const dto = {
        username: `${firstName} ${lastName}`,
        login: faker.internet.userName(),
        password: faker.internet.password(),
        gender: Gender.MALE,
        age: 18,
      };

      const newUserMethodSpy = jest.spyOn(userService, 'newUser');
      const extractFirstNameMethodSpy = jest.spyOn(
        UserService.prototype as any,
        'extractLastName',
      );
      const extractLastNameMethodSpy = jest.spyOn(
        UserService.prototype as any,
        'extractLastName',
      );

      const result = await userController.createNewUser(dto);

      expect(newUserMethodSpy).toHaveBeenCalledTimes(1);
      expect(extractFirstNameMethodSpy).toHaveBeenCalledTimes(1);
      expect(extractLastNameMethodSpy).toHaveBeenCalledTimes(1);

      expect(result.data).toMatchObject(dto);

      expect(result.data.firstName).toEqual(firstName);
      expect(result.data.lastName).toEqual(lastName);

      expect(result.data.id).toBeDefined();
      expect(result.data.createdAt).toBeDefined();
      expect(result.data.updatedAt).toBeDefined();
      expect(result.data.deletedAt).toBeDefined();
    });
  });
});

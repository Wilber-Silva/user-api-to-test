import { Module } from '@nestjs/common';
import { UseCasesModule } from '@UseCases/usecase.module';
import { UserUseCasesModule } from '@UseCases/User/user.usecase.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [UseCasesModule, UserUseCasesModule],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}

import { Test, TestingModule } from '@nestjs/testing';
import faker from 'faker';
import { UserController } from './user.controller';
import { Gender } from '@Enum';
import { UserService } from './user.service';
import { UserUseCasesModule } from '@UseCases/User/user.usecase.module';

describe('UserService', () => {
  let userService: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [UserUseCasesModule],
      controllers: [UserController],
      providers: [UserService],
    }).compile();

    userService = module.get<UserService>(UserService);
  });

  describe('root', () => {
    it('should create and return created user', async () => {
      const firstName = faker.name.firstName();
      const lastName = faker.name.lastName();
      const dto = {
        username: `${firstName} ${lastName}`,
        login: faker.internet.userName(),
        password: faker.internet.password(),
        gender: Gender.MALE,
        age: 18,
      };

      const extractFirstNameMethodSpy = jest.spyOn(
        UserService.prototype as any,
        'extractLastName',
      );
      const extractLastNameMethodSpy = jest.spyOn(
        UserService.prototype as any,
        'extractLastName',
      );

      const result = await userService.newUser(dto);

      expect(extractFirstNameMethodSpy).toHaveBeenCalledTimes(1);
      expect(extractLastNameMethodSpy).toHaveBeenCalledTimes(1);

      expect(extractFirstNameMethodSpy).toHaveBeenCalledWith(dto.username);
      expect(extractLastNameMethodSpy).toHaveBeenCalledWith(dto.username);

      expect(result).toMatchObject(dto);

      expect(result.firstName).toEqual(firstName);
      expect(result.lastName).toEqual(lastName);

      expect(result.id).toBeDefined();
      expect(result.createdAt).toBeDefined();
      expect(result.updatedAt).toBeDefined();
      expect(result.deletedAt).toBeDefined();
    });
  });
});

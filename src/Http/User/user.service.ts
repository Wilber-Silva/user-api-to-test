import { Injectable } from '@nestjs/common';
import { ISearchWithPaginationResults } from '@Protocols/HttpRequest/search-with-pagination-results';
import { IUserModel } from '@Protocols/Model';
import { IUserSchema } from '@Protocols/Schema';
import { CreateUserUsecase } from '@UseCases/User/cases/create-user.usecase';
import { GetOneUserByIdUsecase } from '@UseCases/User/cases/get-one-user-by-id.usecase';
import { GetUsersWithPaginationUsecase } from '@UseCases/User/cases/get-users-with-pagination.usecase';
import { UpdateUserUsecase } from '@UseCases/User/cases/update-user.usecase';
import {
  CreateUserDto,
  SearchUsersWithPaginationDto,
  UpdateUserDto,
} from './DataObjectTransfer';

@Injectable()
export class UserService {
  constructor(
    private readonly createUserUseCase: CreateUserUsecase,
    private readonly updateUserUseCase: UpdateUserUsecase,
    private readonly getOneUserByIdUseCase: GetOneUserByIdUsecase,
    private readonly getUsersWithPaginationUseCase: GetUsersWithPaginationUsecase,
  ) {}
  private extractFirstName(username: string): string {
    return username.split(' ')[0];
  }
  private extractLastName(username: string): string {
    const split = username.split(' ');
    const sliced = split.slice(1, split.length);
    return sliced.join(' ');
  }
  protected separatesUserName(username: string) {
    return {
      firstName: this.extractFirstName(username),
      lastName: this.extractLastName(username),
    };
  }
  private async generateWhere(
    query: SearchUsersWithPaginationDto,
  ): Promise<Partial<IUserModel>> {
    const where: { [key: string]: any } = {};

    const keys = Object.keys(query);

    keys.forEach((key: string) => {
      where[key] = query[key];
    });

    delete where.page;
    delete where.take;

    return where;
  }
  async newUser(userDto: CreateUserDto): Promise<IUserModel> {
    if (!userDto.firstName) {
      userDto.firstName = this.extractFirstName(userDto.username);
    }
    if (!userDto.lastName) {
      userDto.lastName = this.extractLastName(userDto.username);
    }
    return this.createUserUseCase.execute(userDto);
  }

  async updateUserInformation(
    id: string,
    userDto: UpdateUserDto,
  ): Promise<void> {
    if (userDto.username) {
      if (!userDto.firstName) {
        userDto.firstName = this.extractFirstName(userDto.username);
      }
      if (!userDto.lastName) {
        userDto.lastName = this.extractLastName(userDto.username);
      }
    }

    await this.updateUserUseCase.execute({
      id,
      data: userDto,
    });
  }

  async getOneUserById(id: string): Promise<IUserModel> {
    return this.getOneUserByIdUseCase.execute(id);
  }

  async getUsersWithPagination(query: SearchUsersWithPaginationDto) {
    const { page, take } = query;
    const where = await this.generateWhere(query);
    return this.getUsersWithPaginationUseCase.execute({
      page: Number(page),
      take: Number(take),
      where,
    });
  }
}

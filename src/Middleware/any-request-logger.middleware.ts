import { Request, Response, NextFunction } from 'express';
import { LoggerProvider } from '@Providers/Logger/logger.provider';

export function AnyRequestLoggerMiddleware(
  request: Request,
  _: Response,
  next: NextFunction,
) {
  const logger = new LoggerProvider();
  logger.verbose(
    'New Request hes coming',
    JSON.stringify({
      path: request.path,
      method: request.method,
    }),
  );
  next();
}

export interface ISearchWithPaginationResults {
  take: number;
  page: number;
}

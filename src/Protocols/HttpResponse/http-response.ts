export interface IHttpResponse<Data> {
  data: Data;
}

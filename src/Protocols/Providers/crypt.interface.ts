export type CryptValue = string | Buffer;

export interface ICrypt {
  hash(value: CryptValue, saltOrRounds: number): Promise<string>;
  compare(value: CryptValue, encrypted: string): Promise<boolean>;
}

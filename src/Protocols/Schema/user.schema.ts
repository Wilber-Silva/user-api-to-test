import { Gender } from '@Enum';

export interface IUserSchema {
  username: string;
  firstName?: string;
  lastName?: string;
  nickname?: string;
  login: string;
  password?: string;
  gender: Gender;
  age: number;
}

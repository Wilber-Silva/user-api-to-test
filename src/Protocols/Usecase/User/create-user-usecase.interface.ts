import { IUserModel } from '@Protocols/Model';
import { IUserSchema } from '@Protocols/Schema';
import { IUseCaseExecuteCreateDatabase } from '../usecase-execute-create-database.interface';

export interface ICreateUserUsecase
  extends IUseCaseExecuteCreateDatabase<IUserSchema, IUserModel> {
  execute(user: IUserSchema): Promise<IUserModel>;
}

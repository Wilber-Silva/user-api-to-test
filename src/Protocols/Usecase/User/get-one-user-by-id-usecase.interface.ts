import { IUserModel } from '@Protocols/Model';
import { IUseCaseExecute } from '../usecase-execute.interface';

export interface IGetOneUserByIdUseCase
  extends IUseCaseExecute<string, IUserModel> {}

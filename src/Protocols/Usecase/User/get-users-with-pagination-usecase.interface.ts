import { IUserModel } from '@Protocols/Model';
import { IUseCaseExecute } from '../usecase-execute.interface';
import { IPaginationParamUsecase } from '../usecase-pagination-param.interface';
import { IPaginationResultUsecase } from '../usecase-paginate-results.interface';

export interface IGetUserWithPaginationUsecase
  extends IUseCaseExecute<
    IPaginationParamUsecase<IUserModel>,
    IPaginationResultUsecase<IUserModel>
  > {}

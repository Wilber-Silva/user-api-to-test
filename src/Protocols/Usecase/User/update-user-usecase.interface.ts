import { IUserSchema } from '@Protocols/Schema';
import { IUseCaseExecute } from '../usecase-execute.interface';
import { IUpdateParamUseCase } from '../usecase-param-update.interface';

export interface IUpdateUserUseCase
  extends IUseCaseExecute<IUpdateParamUseCase<IUserSchema>, void> {}

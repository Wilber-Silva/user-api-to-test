import { IBaseModel } from '../Model';
import { IUseCaseExecute } from './usecase-execute.interface';

export interface IUseCaseExecuteCreateDatabase<P, R extends IBaseModel>
  extends IUseCaseExecute<P, R> {}

import { IUseCase } from './usecase.interface';

export interface IUseCaseExecute<P, R> extends IUseCase<P, R> {}

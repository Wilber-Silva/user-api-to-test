export interface IPaginationResultUsecase<Collection> {
  take: number;
  page: number;
  count: number;
  collection: Collection[];
}

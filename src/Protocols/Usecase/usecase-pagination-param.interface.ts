type ASC = 'asc';
type DESC = 'desc';

type ORDER = ASC | DESC;

export interface IPaginationParamUsecase<Where> {
  take?: number;
  page: number;
  where?: Partial<Where>;
}

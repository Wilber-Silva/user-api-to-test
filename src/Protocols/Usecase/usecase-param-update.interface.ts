export interface IUpdateParamUseCase<Data> {
  id: string;
  data: Data;
}

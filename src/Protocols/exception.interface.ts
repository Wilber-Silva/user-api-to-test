export interface IError {
  property: string;
  rule?: string;
  message?: string;
  children?: IError[];
}
export interface IException {
  errors?: IError[];
  message: string;
  statusCode?: number;
  stack?: string;
}

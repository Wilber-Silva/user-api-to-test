import { Injectable } from '@nestjs/common';
import { CryptValue, ICrypt } from '@Protocols/Providers';
import * as bcrypt from 'bcrypt';

@Injectable()
export class BcryptAdapter implements ICrypt {
  hash(value: CryptValue, saltOrRounds: number): Promise<string> {
    return bcrypt.hash(value, saltOrRounds);
  }
  compare(value: CryptValue, encrypted: string): Promise<boolean> {
    return bcrypt.compare(value, encrypted);
  }
}

import { Module } from '@nestjs/common';
import { BcryptAdapter } from './bcrypt.adapter';
import { CryptProvider } from './crypt.provider';

@Module({
  imports: [],
  providers: [CryptProvider, BcryptAdapter],
  exports: [CryptProvider],
})
export class CryptModule {}

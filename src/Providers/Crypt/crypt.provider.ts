import { Inject, Injectable } from '@nestjs/common';
import { ICrypt, CryptValue } from '@Protocols/Providers';
import { BcryptAdapter } from './bcrypt.adapter';

@Injectable()
export class CryptProvider {
  private saltOrRounds = 10;

  constructor(@Inject(BcryptAdapter) private readonly cryptAdapter: ICrypt) {}

  async hash(value: CryptValue): Promise<string> {
    return this.cryptAdapter.hash(value, this.saltOrRounds);
  }

  async compare(value: CryptValue, encrypted: string): Promise<boolean> {
    return this.cryptAdapter.compare(value, encrypted);
  }
}

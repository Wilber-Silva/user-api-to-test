import { Scope, Injectable, ConsoleLogger } from '@nestjs/common';
import * as log4js from 'log4js';

@Injectable({ scope: Scope.TRANSIENT })
export class LoggerProvider extends ConsoleLogger {
  private logger: log4js.Logger;
  constructor() {
    super();
    const type = process.env.LOGGER_TYPE || 'console';
    const level = process.env.LOGGER_LEVEL || 'debug';
    log4js.configure({
      appenders: {
        console: { type: 'console' },
        file: { type: 'file' },
      },
      categories: { default: { appenders: [type], level } },
    });
    this.logger = log4js.getLogger();
  }

  public error(message: string, stack?: string, context?: any) {
    super.error(message, stack, context);
    this.logger.error(message, stack);
  }

  public debug(message: string, context?: any) {
    super.debug(message, context);
    this.logger.debug(message, context);
  }

  public verbose(message: string, context?: any) {
    super.verbose(message, context);
    this.logger.info(message, context);
  }

  public log(message: string, context?: any) {
    super.log(message, context);
    this.logger.info(message, context);
  }
}

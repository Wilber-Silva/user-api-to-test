import { UserRepository } from '@Database/Repositories';
import { UnprocessableEntityException } from '@Exceptions';
import { Injectable } from '@nestjs/common';
import { IUserModel } from '@Protocols/Model';
import { IUserSchema } from '@Protocols/Schema';
import { ICreateUserUsecase } from '@Protocols/Usecase';
import { CryptProvider } from '@Providers/Crypt/crypt.provider';

@Injectable()
export class CreateUserUsecase implements ICreateUserUsecase {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly cryptProvider: CryptProvider,
  ) {}

  async execute(userToCreate: IUserSchema): Promise<IUserModel> {
    const exists = await this.userRepository.findOne({
      where: {
        login: userToCreate.login,
      },
    });
    if (exists) {
      throw new UnprocessableEntityException({
        message: 'User already exists',
      });
    }
    if (userToCreate.password) {
      userToCreate.password = await this.cryptProvider.hash(
        userToCreate.password,
      );
    }
    const createdUser = this.userRepository.create(userToCreate);
    return this.userRepository.save(createdUser);
  }
}

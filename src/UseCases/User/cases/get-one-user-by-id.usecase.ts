import { UserRepository } from '@Database/Repositories';
import { NotfoundException } from '@Exceptions';
import { Injectable } from '@nestjs/common';
import { IUserModel } from '@Protocols/Model';
import { IGetOneUserByIdUseCase } from '@Protocols/Usecase';

@Injectable()
export class GetOneUserByIdUsecase implements IGetOneUserByIdUseCase {
  constructor(private readonly userRepository: UserRepository) {}

  async execute(userId: string): Promise<IUserModel> {
    const user = await this.userRepository.findOne({
      where: { id: userId },
    });
    if (!user) {
      throw new NotfoundException({ message: 'User not found' });
    }
    return user;
  }
}

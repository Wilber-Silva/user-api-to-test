import { UserRepository } from '@Database/Repositories';
import { Injectable } from '@nestjs/common';
import { IUserModel } from '@Protocols/Model';
import {
  IGetUserWithPaginationUsecase,
  IPaginationResultUsecase,
  IPaginationParamUsecase,
} from '@Protocols/Usecase';
@Injectable()
export class GetUsersWithPaginationUsecase
  implements IGetUserWithPaginationUsecase
{
  constructor(private readonly userRepository: UserRepository) {}

  async execute(
    params: IPaginationParamUsecase<IUserModel>,
  ): Promise<IPaginationResultUsecase<IUserModel>> {
    const take = params.take || 10;
    const page = params.page || 1;
    const skip = (page - 1) * take;
    const [users, count] = await this.userRepository.findAndCount({
      take,
      skip,
      where: params.where,
    });
    return {
      take,
      page,
      count,
      collection: users,
    };
  }
}

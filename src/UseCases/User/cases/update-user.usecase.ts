import { UserRepository } from '@Database/Repositories';
import { NotfoundException, UnprocessableEntityException } from '@Exceptions';
import { Injectable } from '@nestjs/common';
import { IUserSchema } from '@Protocols/Schema';
import { IUpdateParamUseCase, IUpdateUserUseCase } from '@Protocols/Usecase';
import { CryptProvider } from '@Providers/Crypt/crypt.provider';
import { Not } from 'typeorm';

@Injectable()
export class UpdateUserUsecase implements IUpdateUserUseCase {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly cryptProvider: CryptProvider,
  ) {}

  async execute(params: IUpdateParamUseCase<IUserSchema>): Promise<void> {
    const exists = await this.userRepository.findOne({
      where: {
        id: Not(params.id),
        login: params.data.login,
      },
    });
    if (exists) {
      throw new UnprocessableEntityException({
        message: 'User already exists',
      });
    }

    if (params.data.password) {
      params.data.password = await this.cryptProvider.hash(
        params.data.password,
      );
    }

    const result = await this.userRepository.update(params.id, params.data);

    if (result.affected === 0) {
      throw new NotfoundException({ message: 'User not found' });
    }
  }
}

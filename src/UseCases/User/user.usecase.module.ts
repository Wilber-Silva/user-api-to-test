import { DatabaseModule } from '@Database/database.module';
import { Module } from '@nestjs/common';
import { CryptModule } from '@Providers/Crypt/crypt.module';
import { CreateUserUsecase } from './cases/create-user.usecase';
import { GetOneUserByIdUsecase } from './cases/get-one-user-by-id.usecase';
import { GetUsersWithPaginationUsecase } from './cases/get-users-with-pagination.usecase';
import { UpdateUserUsecase } from './cases/update-user.usecase';
@Module({
  imports: [DatabaseModule, CryptModule],
  providers: [
    CreateUserUsecase,
    UpdateUserUsecase,
    GetOneUserByIdUsecase,
    GetUsersWithPaginationUsecase,
  ],
  exports: [
    CreateUserUsecase,
    UpdateUserUsecase,
    GetOneUserByIdUsecase,
    GetUsersWithPaginationUsecase,
  ],
})
export class UserUseCasesModule {}

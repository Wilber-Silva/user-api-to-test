import { DatabaseModule } from '@Database/database.module';
import { Module } from '@nestjs/common';
import { UserUseCasesModule } from './User/user.usecase.module';
@Module({
  imports: [DatabaseModule, UserUseCasesModule],
})
export class UseCasesModule {}

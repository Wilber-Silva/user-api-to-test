import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AnyRequestLoggerMiddleware } from './Middleware';
import { ConfigModule } from 'nestjs-dotenv';
import { Modules } from './modules';
import { APP_GUARD } from '@nestjs/core';
import { LoggerProvider } from '@Providers/Logger/logger.provider';
import { AuthGuard } from './Guard';
@Module({
  imports: [ConfigModule.forRoot(), ...Modules],
  providers: [{ provide: APP_GUARD, useClass: AuthGuard }, LoggerProvider],
  exports: [LoggerProvider],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    Modules.forEach(module => {
      consumer.apply(AnyRequestLoggerMiddleware).forRoutes(module);
    });
  }
}

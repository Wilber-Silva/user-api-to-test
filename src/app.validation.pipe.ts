import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToInstance } from 'class-transformer';
import { BadRequestException } from '@Exceptions/badrequest.exception';
import { IError } from '@Protocols/exception.interface';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (metatype) {
      const object = plainToInstance(metatype, value);
      await this.validate(object, metatype);
    } else {
      await this.validate(value, metatype);
    }
    return value;
  }

  private async validate(value: any, metatype?: any): Promise<void> {
    const errors = await validate(value, { transform: true });
    if (errors.length > 0) {
      throw new BadRequestException({
        message: 'Validation failed',
        errors: errors.map(error => {
          let rule: string = 'unexpected';
          let message: string = error.toString();
          let property: string = error.property;
          const children: IError[] = [];

          if (error.constraints) {
            const constraintsError = error.constraints;
            const constraintErrorKeys = Object.keys(constraintsError);

            constraintErrorKeys.forEach(key => {
              children.push({
                rule: key,
                message: constraintsError[key],
                property: error.property,
              });
            });
          }

          if (error.children && error.children.length) {
            error.children.forEach(child => {
              if (child.constraints) {
                const constraints = child.constraints;
                const constraintKeys = Object.keys(constraints);

                constraintKeys.forEach(key => {
                  children.push({
                    rule: key,
                    message: constraints[key],
                    property: child.property,
                  });
                });
              }
            });
          }
          return {
            rule,
            property,
            message,
            children,
          };
        }),
        stack: `ValidationPipe ${metatype}`,
      });
    }
  }
}

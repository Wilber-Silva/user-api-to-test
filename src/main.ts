import { NestFactory } from '@nestjs/core';
import { INestApplication } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { json, urlencoded } from 'body-parser';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';
import { ValidationPipe } from 'app.validation.pipe';
import { LoggerProvider } from '@Providers/Logger/logger.provider';

dotenv.config();

var logger = new LoggerProvider();

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger,
  });
  addSwaggerConfig(app);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ limit: '50mb', extended: true }));

  const App_Port: number = Number(process.env.SERVER_PORT);

  await app.listen(App_Port);

  logger.log(
    'Api has Started',
    JSON.stringify({
      url: `localhost:${App_Port}`,
      port: App_Port,
    }),
  );
}
function addSwaggerConfig(app: INestApplication) {
  const config = new DocumentBuilder()
    .setTitle('API Test')
    .setDescription('API')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
}

bootstrap();
